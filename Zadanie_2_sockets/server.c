/*Server*/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <signal.h>		//SIGCHLD, SIG_IGN
#include <sys/types.h>
#include <time.h>
#include <math.h>

int main(){
  int serverSocket, clientSocket, endian;
  int x; 							//zmienna odpowiedzialna za endiana
  int conv=0;						//zmiana endian
  int a,b,c,w;						//zmienne do komunikacji danych z clientem
	float delta,x0,x1,x2;		//zmienne rownania kwadratowego
	int pi;									//ilosc pierwiastkow rownania
	pid_t pid;							//process id
  time_t timer;
  time(&timer);
  	  	  	  //sockaddr_in structure used with IPv4
  struct sockaddr_in serverAddress;
  struct sockaddr_storage serverStorage;
  	  	  	  //stores socket address information
  socklen_t clientLength ;

  	  	  	  //    tworzymy sockety z trzeba argumentami
  	  	  	  //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP)
  serverSocket = socket(PF_INET, SOCK_STREAM, 0);

  	  	  	  //    konfigurowanie opcji adresu servera (struct)
  	  	  	  //    adres family = Internet
  serverAddress.sin_family = AF_INET;

  	  	  	  //    ustawianie numeru portu przez  htons (kole  //    tworzymy sockety z trzeba argumentami
  	  	  	  //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP in this case) jność bajtów)
  serverAddress.sin_port = htons(5555);

  	  	  	  //    IP na localhost
  serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1");

  	  	  	  //    ustawienie bitów 0
  memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);

  	  	  	  //    powiązanie adresu struktury z socketem
  bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress));

  	  	  	  //    Nasłuchiwanie (socket) maksymalnie 5 połączeń
  listen(serverSocket,5);

  signal (SIGCHLD, SIG_IGN);



							//sprawdzanie endniana
											union {
														short s;								//deklaracja musi by tu
													  char c[sizeof(short)];	//deklaracja musi by tu
												 }un;
												 un.s = 0x0102;

												 if (sizeof(short) == 2) {
														if (un.c[0] == 1 && un.c[1] == 2)
															 x=1; //big endian

														else if (un.c[0] == 2 && un.c[1] == 1)
															 x=0; //little endian

														else
															 printf("error\n");
														}
								 		 endian = x;   //od teraz wiem z czym mam doczynienia ;P



  	  while(1){			//biedak siedzi w petli i nigdy nie wyjdzie

  		  	  //Nowe połączenie
  		   clientLength = sizeof(serverStorage);
  		   clientSocket = accept (serverSocket, (struct sockaddr *) &serverStorage, &clientLength);
					
					switch ((pid = fork ())) { 		//obsługa możliwości
					case -1: // blad 
						break;
					case 0:  // Proces potomny 
					{
						while(1){		
																//uzywanie read  -> ssize_t read(int fd, void *buf, size_t count);

										if((read(clientSocket, &w,sizeof(w)))==-1){
											perror("Error");
										}

										if(w == 1){
											write(clientSocket, ctime(&timer), 24);
										}else if(w == 2){
											if((read(clientSocket, &a,sizeof(a)))==-1){
												perror("Error");
									         			
											}
											if((read(clientSocket, &b,sizeof(b)))==-1){
												perror("Error");
															
											}
											if((read(clientSocket, &c,sizeof(c)))==-1){
												perror("Error");
															
											}


											delta = ((float)b*(float)b) - (4*(float)a*(float)c);

											if(delta < 0){
												pi=0;
											}
											else if(delta == 0){
												pi=1;
											}
											else{
												pi=2;
											}

											//konwersja endian

												if(endian==0){

													char *oldendian = (char *)&pi;
													char *newendian = (char *)&conv;
													newendian[0] = oldendian[3];
													newendian[1] = oldendian[2];
													newendian[2] = oldendian[1];
													newendian[3] = oldendian[0];
													pi=conv;
												}
									

											write(clientSocket, &conv, sizeof(pi)); //wysylanie ilosci rozwiazan rownania kwadratowego

											if(delta > 0){
												x1=(((float)b*(-1))-sqrt(delta))/(2*(float)a);
												write(clientSocket, &x1, sizeof(x1));

												x2=(((float)b*(-1))+sqrt(delta))/(2*(float)a);
												write(clientSocket, &x2, sizeof(x2));


											}
											else if(delta == 0){

												x0=((float)b*(-1))/(2*(float)a);
												write(clientSocket, &x0, sizeof(x0));

											}

										}


					}}
						break;
					default: //Proces rodzicielski
						close (clientSocket);
						break;
					}


  	  }

  return 0;
}
