/*Client*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(){
  int clientSocket, result, endian;
 	int a,b,c,w;						//zmienne do komunikacji
  int x; 							//zmienna odpowiedzialna za endiana
	float x0,x1,x2;		//zmienne rownania kwadratowego
	int pi;						//ilosc pierwiastkow rownania
	int bendpi; 			//ile pierwiastkow przed uzyciem endian
  socklen_t length;
  struct sockaddr_in serverAddress;
  socklen_t addr_size;

    //    tworzymy sockety z trzeba argumentami dla klienta
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP)
  clientSocket = socket(PF_INET, SOCK_STREAM, 0);

    //    konfigurowanie opcji adresu servera (struct) - nazwy
    //    adres family = Internet
  serverAddress.sin_family = AF_INET;

    //    ustawianie numeru portu przez  htons (kole  //    tworzymy sockety z trzeba argumentami
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP in this case) jność bajtów)
  serverAddress.sin_port = htons(5555);

    //    IP na localhost
  serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1");

    //    ustawienie bitów 0
  length = sizeof (serverAddress);
  memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);

    //    powiązanie adresu struktury z socketem
  addr_size = sizeof serverAddress;
  //connect(clientSocket, (struct sockaddr *) &serverAddress, addr_size);
  result = connect (clientSocket, (struct sockaddr *) &serverAddress, length);

  	if (result == -1)
  	{
  		perror ("Error with connection");
  		exit (0);
  	}
								//endiany
						union {
								  short s;
								  char c[sizeof(short)];

							 }un;
							 un.s = 0x0102;

							 if (sizeof(short) == 2) {
								  if (un.c[0] == 1 && un.c[1] == 2)
								     x=1; //big endian

								  else if (un.c[0] == 2 && un.c[1] == 1)
								     x=0; //little endian

								  else
								     printf("error\n");
								  }
						endian = x;
	
  	while(1){  //aby zabawa trwala w nieskonczonosc

  			printf("Wybierz cyfre: \n");
				printf("1. Wyswietl czas.\n");
				printf("2. Oblicz rownanie kwadratowe.\n");
				printf("3. Koniec.\n");

  			scanf("%d",&w);

  			write(clientSocket, &w, sizeof(w));

  			if(w == 1){       //podawanie czasu
  				char czas[24]="";
  				read(clientSocket, &czas,24);
  				printf ("Czas = %s\n", czas);
  			}else if(w == 2){		//rownanie kwadratowe

  				printf("liczba A : ");
  				scanf("%d",&a);
  				write(clientSocket, &a, sizeof(a));

  				printf("liczba B : ");
  				scanf("%d",&b);
  				write(clientSocket, &b, sizeof(b));

  				printf("liczba C : ");
  				scanf("%d",&c);
  				write(clientSocket, &c, sizeof(c));	

  				read(clientSocket, &bendpi,sizeof(bendpi)); //ile pierwiastkow

  				//endian
						if(endian==0){

							char *oldendian = (char *)&bendpi;
							char *newendian = (char *)&pi;
							newendian[0] = oldendian[3];
							newendian[1] = oldendian[2];
							newendian[2] = oldendian[1];
							newendian[3] = oldendian[0];

						}

  				if(pi == 0){
  					printf("\nDelta ujemna. Brak rozwiazan.\n\n");
  				}else if(pi == 1){
  					read(clientSocket, &x0,sizeof(x0));
  					printf("\nRozwiazanie : x0 = %4.2f \n",x0);
  				}else if(pi == 2){
  					read(clientSocket, &x1,sizeof(x1));
  					read(clientSocket, &x2,sizeof(x2));
  					printf("\nRozwiazania : x1 = %4.2f : x2 = %4.2f \n", x1,x2);
  				}else{printf("Server error\n");
					}

  			}else if(w == 3){ // zakonczenie programu przez uzytkownika
					printf("BYE\n");
				exit(1);
			}
		}

  return 0;
}
