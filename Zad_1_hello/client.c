/*Client*/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(){
  int clientSocket;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

    //    tworzymy sockety z trzeba argumentami
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP) 
  clientSocket = socket(PF_INET, SOCK_STREAM, 0);
  
    //    konfigurowanie opcji adresu servera (struct)
    //    adres family = Internet
  serverAddr.sin_family = AF_INET;
  
    //    ustawianie numeru portu przez  htons (kole  //    tworzymy sockety z trzeba argumentami
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP in this case) jność bajtów)
  serverAddr.sin_port = htons(7891);
  
    //    IP na localhost
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
    //    ustawienie bitów 0
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

    //    powiązanie adresu struktury z socketem
  addr_size = sizeof serverAddr;
  connect(clientSocket, (struct sockaddr *) &serverAddr, addr_size);

    //odczyanie wiadomości w bufor
  recv(clientSocket, buffer, 1024, 0);

    //wydrukuj
  printf("Data received: %s",buffer);   

  return 0;
}
