/*Server*/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(){
  int welcomeSocket, newSocket;
  char buffer[1024];
  struct sockaddr_in serverAddr;
  struct sockaddr_storage serverStorage;
  socklen_t addr_size;

    //    tworzymy sockety z trzeba argumentami
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP) 
  welcomeSocket = socket(PF_INET, SOCK_STREAM, 0);
  
    //    konfigurowanie opcji adresu servera (struct)
    //    adres family = Internet
  serverAddr.sin_family = AF_INET;
  
    //    ustawianie numeru portu przez  htons (kole  //    tworzymy sockety z trzeba argumentami
    //    1) Domena internetowa 2) strumien socketu 3) Default protokół (TCP in this case) jność bajtów)
  serverAddr.sin_port = htons(7891);
  
    //    IP na localhost
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
    //    ustawienie bitów 0
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

    //    powiązanie adresu struktury z socketem
  bind(welcomeSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

    //    Nasłuchiwanie (socket) maksymalnie 5 połączeń
  if(listen(welcomeSocket,5)==0)
    printf("Listening\n");
  else
    printf("Error\n");

    //    nowy socket dla połączeń przychodzących, przyjmowanie połaczeń
  addr_size = sizeof serverStorage;
  newSocket = accept(welcomeSocket, (struct sockaddr *) &serverStorage, &addr_size);

    //    powitanie dla nowego socketu połączeń
  strcpy(buffer,"Hello World\n");
  send(newSocket,buffer,13,0);

  return 0;
}